﻿using System;
//Almost whole week was in high fever, so didn't physically have much time and possibilities to put in the tasks :( 
/* F1 Create these classes and a program that uses them */
/* car - owner from the point of view of parking control system. */
namespace taskF1A
{
   
    class Owner
    {
        

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OwnerID { get; set; }

        public Owner(string firstName, string lastName, string ownerID)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.OwnerID = ownerID;
        }

        public void GetInfo()
        {
            Console.WriteLine($"Owner's name : {FirstName} Surname: {LastName} ID : {OwnerID}");
        }

    }

    class Car
    {
     

        public string NumberPlate { get; set; }
        public bool Permission { get; set; }
        public string Model { get; set; }
        public string OwnerID { get; set; }

        public Car(string numberPlate, bool permission, string model, string ownerID)
        {
            this.NumberPlate = numberPlate;
            this.Permission = permission;
            this.Model = model;
            this.OwnerID = ownerID;
        }

        public void GetInfo()
        {
            Console.WriteLine($"Car's number : {NumberPlate}, Owner: {OwnerID},  Model: {Model} , Permission : {Permission}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Owner person1 = new Owner("Vano", "Lepezk", "D907A");
            person1.GetInfo();

            Car car1 = new Car("xyz880", true, "Vaz2107", "D907A");
            car1.GetInfo();
        }


        
    }

   
}
