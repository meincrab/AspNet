﻿using System;
//vehicle - car classes
//Car Derived from Vehicle.
namespace taskF1F
{
    class Vehicle
    {
        protected float _maxSpeed;
        protected int _horsePowers;
        protected float _weight;
        protected float _maxLoad;
        protected int _maxPassangers;

        public Vehicle(float maxSpeed, int horsePowers, float weight, float maxLoad, int maxPassangers)
        {
            _maxSpeed = maxSpeed;
            _horsePowers = horsePowers;
            _weight = weight;
            _maxLoad = maxLoad;
            _maxPassangers = maxPassangers;
        }
        public virtual void DisplayInfo()
        {
            Console.WriteLine($"Max Speed: {_maxSpeed}, Max Load : {_maxLoad}, Max Passengers : {_maxPassangers}, Horse Powers: {_horsePowers}, Weight: {_weight}");
        }
    }
    class Car:Vehicle
    {
        private int _wheelAmount;
        private float _currentSpeed;

        public Car(int wheelAmount, float currentSpeed, float maxSpeed, int horsePowers, float weight, float maxLoad, int maxPassangers)
            :base(maxSpeed, horsePowers, weight, maxLoad, maxPassangers)
        {
            _wheelAmount = wheelAmount;
            _currentSpeed = currentSpeed;
        }

        public void IncreaseSpeed()
        {
            _currentSpeed += 10;
            
            if (_currentSpeed >= 220)
            {
                _currentSpeed = 220;
                Console.WriteLine("You reached the max speed");
            }
            else
            {
                Console.WriteLine("Current speed is : " + _currentSpeed);
            }
        }
        public void DecreaseSpeed()
        {
            _currentSpeed -= 10;
            if (_currentSpeed <= 0)
            {
                _currentSpeed = 0;
                Console.WriteLine("Auto stopped");
            }
            else
            {
                Console.WriteLine("Current speed is : " + _currentSpeed);
            }
          
        }
        public float DisplaySpeed()
        {
            return _currentSpeed;
        }
        public override void DisplayInfo()
        {
            Console.WriteLine($"Max Speed: {_maxSpeed}, Max Load : {_maxLoad}, Max Passengers : {_maxPassangers}, Horse Powers: {_horsePowers}, Weight: {_weight}, Wheel amount : {_wheelAmount}, Current speed : {_currentSpeed}");
        }
        
    }
    class Program
    {
        static void Main(string[] args)
        {
            Car rustBucket = new Car(4, 0f, 220, 100, 900, 1000, 4);
            while (true)
            {
                Console.WriteLine("Do you want to speed up or slow down? Your current speed is : " + rustBucket.DisplaySpeed());
                Console.WriteLine("Press 1 to speed up , press 2 to slow down, press 3 to show info" );
                int choice;
                while (!int.TryParse(Console.ReadLine(), out choice) && choice > 0 && choice < 3)
                    Console.WriteLine("Try again");
                switch (choice)
                {
                    case 1:
                        rustBucket.IncreaseSpeed();
                        break;
                    case 2:
                        rustBucket.DecreaseSpeed();
                        break;
                    case 3:
                        rustBucket.DisplayInfo();
                        break;
                }

            }

        }
    }
}
