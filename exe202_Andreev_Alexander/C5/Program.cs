﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*C5 Develop application solving a linear equation in form Ax + B = 0. Value of terms shall be read from the standard input.*/
namespace C5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the values for equation Ax+B=0, First Value A, Second Value B and third is a Result");
            Console.WriteLine("Value A: ");
            float valueA = float.Parse(Console.ReadLine());
            Console.WriteLine("Value B: ");
            float valueB = float.Parse(Console.ReadLine());
            Console.WriteLine("Result : ");
            float valueResult = float.Parse(Console.ReadLine());

            float valueX = (valueResult - valueB) / valueA;
            Console.WriteLine("X value is : " + valueX);
            Console.ReadLine();

        }
    }
}
