﻿using System;
/*Program tells which is the biggest out of 3 given integers.*/
namespace C1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter 3 integers, pressing ENTER after each input");
            int[] userInput = new int[3];
            int biggestNumber = 0;
            for (int i = 0; i < userInput.Length; i++)
            {
                userInput[i] = int.Parse(Console.ReadLine());
            }
            for(int i = 0; i < userInput.Length; i++)
            {
                if (biggestNumber <= userInput[i]) biggestNumber = userInput[i];
            }
            Console.WriteLine("the biggest number out of three: " + biggestNumber);
            Console.ReadKey();
        }
    }
}
