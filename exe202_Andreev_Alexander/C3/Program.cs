﻿using System;
/* 
C3 Classification of Triangles

A triangle can be classified from two points of view: (1) relation of lengths of edges and (2) relation of angels between edges.
In terms of edges, triangle can be equilateral, isosceles and scalene while in terms of angle it can be acute, right and obtuse. 
Develop an application that shall read from the standard input the length of edges of a triangle and make classification from both points of view.
Next, application shall compute the total area of triangle using Heron’s formula. 
*/
namespace C3
{
    class Program
    { 
        static void Main(string[] args)
        {
            Console.WriteLine("Please, enter dimensions of the triangle");
            Console.WriteLine("First Side: ");
            double sideA = double.Parse(Console.ReadLine());
            Console.WriteLine("Second Side: ");
            double sideB = double.Parse(Console.ReadLine());
            Console.WriteLine("Third Side: ");
            double sideC = double.Parse(Console.ReadLine());


            double sideAsquare = sideA * sideA;
            double sideBsquare = sideB * sideB;
            double sideCsquare = sideC * sideC;

            //First we are checking if triangle even exists
            if (sideA + sideB <= sideC || sideA + sideC <= sideB || sideB + sideC <= sideA)
            {
                Console.WriteLine("Triangle doesn't exists");
            }
            else
            {
                //Type of triangle
                if (sideAsquare == sideBsquare + sideCsquare || sideBsquare == sideAsquare + sideCsquare || sideCsquare == sideAsquare + sideBsquare)
                {
                    Console.WriteLine("Right");
                }
                else if (sideAsquare > sideBsquare + sideCsquare || sideBsquare > sideAsquare + sideCsquare || sideCsquare > sideAsquare + sideBsquare)
                {
                    Console.WriteLine("Obtuse");
                }
                else
                {
                    Console.WriteLine("Acute");
                }
                //Second type of triangle
                if (sideA == sideB && sideB == sideC)
                {
                    Console.WriteLine("This is equilateral, triangle");
                    Console.WriteLine("Equilateral can only be acute");
                }
                else if (sideA == sideB || sideA == sideC || sideB == sideC)
                {
                    Console.WriteLine("Two sides are equal, so this is isosceles triangle");
  
                }
                else
                {
                    Console.WriteLine("Triangle is scalene");
                }

                double trianglePerimeter = sideA + sideB + sideC;
                double triangleArea = Math.Sqrt(trianglePerimeter * (trianglePerimeter - sideA) * (trianglePerimeter - sideB) * (trianglePerimeter - sideC));
                Console.WriteLine("Area is "+ Math.Round(triangleArea, 3));

            }
            Console.ReadLine();
        }
    }
}
