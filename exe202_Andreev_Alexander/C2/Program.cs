﻿using System;
/* C2 Program tells if the given value is between 0 and 10.F */
namespace C2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter number between 0 and 10");
            double userInput = Double.Parse(Console.ReadLine());
            if(userInput >= 0 && userInput <= 10)
            {
                Console.WriteLine("Your input is between 0 and 10");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Your input is not between 0 and 10");
                Console.ReadKey();
            }
        }
    }
}
