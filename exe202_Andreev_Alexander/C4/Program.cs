﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*C4 Develop application to display the number of days in the given month. The number of a month shall be read from the standard input.*/
namespace C4
{
    class Program
    {
        public static void DaysFebruary()
        {
            Console.WriteLine("Please enter current year");
            int year = UInt16.Parse(Console.ReadLine());
            bool leapYear = false;
            //First Let's check if now is a leap year
            /*if (year is not divisible by 4) then (it is a common year)
            * else if (year is not divisible by 100) then (it is a leap year)
            * else if (year is not divisible by 400) then (it is a common year)
            * else (it is a leap year)
            * Alogithm found from Wikipedia 
            */
            if (year % 4 != 0)
            {
                Console.WriteLine("Common year");
            }
            else if (year % 100 != 0)
            {
                Console.WriteLine("Leap year");
                leapYear = true;
            }
            else if (year % 400 != 0)
            {
                Console.WriteLine("Common year");
            }
            else
            {
                Console.WriteLine("Leap Year");
                leapYear = true;
            }

            if (leapYear)
            {
                Console.WriteLine("This month has 29 days");
            }
            else
            {
                Console.WriteLine("This month has 28 days");
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the number of the month");
            int monthNumber = UInt16.Parse(Console.ReadLine());
            switch (monthNumber)
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    Console.WriteLine("This month has 31 days");
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    Console.WriteLine("This month has 30 days");
                    break;
                case 2:
                    DaysFebruary();
                    break;


            }

            Console.ReadKey();
            
      
        }

        
    }
}
