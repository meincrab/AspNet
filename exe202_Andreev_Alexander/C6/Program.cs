﻿using System;
using System.Collections;

/* 
C6 Program calculates the sum of numbers 1, 6, 11, … 46. 
*/
namespace C6
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList numbers = new ArrayList();
            Console.WriteLine("Enter numbers, dividing each number with enter. To stop input press enter without entering number ");
            bool goOn = true;
            while (goOn)
            {
                string userInput = Console.ReadLine();
                if (string.IsNullOrEmpty(userInput))
                {
                    Console.WriteLine("End of your input");
                    goOn = false;
                }
                else
                {
                    double userInputNum = double.Parse(userInput);
                    numbers.Add(userInputNum);
                }
            }
            double sum = 0;
            foreach(double number in numbers)
            {
                sum += number;
            }
            Console.WriteLine("Sum of your numbers : " + sum);

        }
    }
}
