﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="task3.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 175px;
        }
        .auto-style2 {
            width: 175px;
            height: 30px;
        }
        .auto-style3 {
            height: 30px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table style="width:100%;">
            <tr>
                <td class="auto-style1">
                    Name</td>
                <td>
                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtName" ErrorMessage="Name Missing"></asp:RequiredFieldValidator>
                </td>

            </tr>
            <tr>
                <td class="auto-style1">
                    Surname</td>
                <td>
                    <asp:TextBox ID="txtSurname" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSurname" ErrorMessage="Surname Missing"></asp:RequiredFieldValidator>
                </td>

            </tr>
            <tr>
                <td class="auto-style1">
                    Password</td>
                <td>
                    <asp:TextBox ID="txtPass" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPass" ErrorMessage="Password Missing"></asp:RequiredFieldValidator>
&nbsp;&nbsp;&nbsp;&nbsp; </td>

            </tr>
             <tr>
                <td class="auto-style2">
                    Re-Password</td>
                <td class="auto-style3">
                    <asp:TextBox ID="txtPassR" runat="server" AutoCompleteType="Cellular" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPassR" ErrorMessage="Password Check Missing"></asp:RequiredFieldValidator>
&nbsp;&nbsp;&nbsp;
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPass" ControlToValidate="txtPassR" ErrorMessage="Passwords don't match"></asp:CompareValidator>
                 </td>

            </tr>
             <tr>
                <td class="auto-style2">
                    Email</td>
                <td class="auto-style3">
                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email Missing"></asp:RequiredFieldValidator>
&nbsp;&nbsp;
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                 </td>

            </tr>
             <tr>
                <td class="auto-style1">
                    Phone Number</td>
                <td>
                    <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtPhone" ErrorMessage="Phone Number Missing"></asp:RequiredFieldValidator>
&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtPhone" ErrorMessage="Invalid Phone Number" ValidationExpression="^\+[1-9]{1}[0-9]{3,14}$"></asp:RegularExpressionValidator>
                 </td>

            </tr>
             <tr>
                <td class="auto-style1">
                    <asp:Button ID="Button1" runat="server" Text="Validate" />
                 </td>
                <td>&nbsp;</td>

            </tr>
        </table>
    </form>
</body>
</html>
