﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MorseCoder.aspx.cs" Inherits="task2.MorseCoder" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Text / Morse Code : "></asp:Label>
            <asp:TextBox ID="morseInput" runat="server" Width="706px"></asp:TextBox>
        </div>
        <asp:Button ID="morseHandler" runat="server" OnClick="morseHandler_Click" Text="Code/Decode" />
        <p>
            <asp:TextBox ID="result" runat="server" OnTextChanged="result_TextChanged" Width="820px">Result</asp:TextBox>
        </p>
    </form>
</body>
</html>
