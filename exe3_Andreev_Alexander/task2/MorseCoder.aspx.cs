﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace task2
{
    public partial class MorseCoder : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        static Dictionary<string, char> dictionary = new Dictionary<string, char>()
            {
                { ".-", 'a' },
                { "-...",'b'},
                { "-.-.",'c'},
                { "-..", 'd'},
                { ".", 'e'},
                { "..-.",'f'},
                { "--.", 'g'},
                { "....",'h'},
                { "..", 'i'},
                { ".---",'j'},
                { "-.-", 'k'},
                { ".-..",'l'},
                { "--", 'm'},
                { "-.", 'n'},
                { "---", 'o'},
                { ".--.",'p'},
                { "--.-",'q'},
                { ".-.", 'r'},
                { "...", 's'},
                { "-", 't'},
                { "..-", 'u'},
                { "...-",'v'},
                { ".--", 'w'},
                { "-..-",'x'},
                { "-.--",'y'},
                { "--..",'z'},
                { ".----",'1'},
                { "..---", '2'},
                { "...--", '3'},
                { ".....", '4'},
                { "-....", '5'},
                { "--...",'6'},
                { "---..", '7'},
                { "----..",'8'},
                { "----.",'9'},
                { "-----",'0'}
            };
       Dictionary<char, string> dictInverse = dictionary.ToDictionary((i) => i.Value, (i) => i.Key);

        protected void morseHandler_Click(object sender, EventArgs e)
        {
            string usrInput = morseInput.Text;
            string output = "";
            if (Regex.IsMatch(usrInput, @"^[a-zA-Z0-9]+$"))
            {
                usrInput = usrInput.ToLower();
                 foreach(char c in usrInput)
                {
                    output += (dictInverse[c] + "   ");
                } 

                result.Text = output;
            }
            else
            {
                string usrInputMorse = usrInput;
                string[] morseLetters = usrInputMorse.Split(' ');

                foreach(string letter in morseLetters)
                {
                    output += dictionary[letter];
                }
                result.Text = output;
            }
            
        }

        protected void result_TextChanged(object sender, EventArgs e)
        {

        }
    }
}