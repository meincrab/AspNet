﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace task1
{
    public partial class Calculator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnSum_Click(object sender, EventArgs e)
        {
            float sum = float.Parse(usrInput1.Text) + float.Parse(usrInput2.Text);
            txtResult.Text = Convert.ToString(sum);
        }

        protected void btnMinus_Click(object sender, EventArgs e)
        {
            float diff = float.Parse(usrInput1.Text) - float.Parse(usrInput2.Text);
            txtResult.Text = Convert.ToString(diff);
        }

        protected void btnMultiply_Click(object sender, EventArgs e)
        {
            float diff = float.Parse(usrInput1.Text) * float.Parse(usrInput2.Text);
            txtResult.Text = Convert.ToString(diff);
        }

        protected void btnDivide_Click(object sender, EventArgs e)
        {
            float divide = float.Parse(usrInput1.Text) / float.Parse(usrInput2.Text);
            txtResult.Text = Convert.ToString(divide);
        }

        protected void btnClr_Click(object sender, EventArgs e)
        {
            usrInput1.Text = "";
            usrInput2.Text = "";
        }

        protected void usrInput1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void usrInput2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}