﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Calculator.aspx.cs" Inherits="task1.Calculator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #Text1 {
            margin-bottom: 0px;
        }
    </style>
</head>
<body>
    <form id="calc" runat="server">
        <div>
             <%-- Using Regex to allow only numbers be in input--%>  
            <asp:Label ID="Label1" runat="server" Text="Number 1 :"></asp:Label>
            <asp:TextBox ID="usrInput1" runat="server" OnTextChanged="usrInput1_TextChanged"></asp:TextBox>  
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                ControlToValidate="usrInput1" runat="server"
                ErrorMessage="Only Numbers allowed"
                ValidationExpression="^[1-9]\d*(\.\d+)?$" />
            <asp:Label ID="Label2" runat="server" Text="Number 2 : "></asp:Label>
            <asp:TextBox ID="usrInput2" runat="server" OnTextChanged="usrInput2_TextChanged" style="height: 22px"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                ControlToValidate="usrInput1" runat="server"
                ErrorMessage="Only Numbers allowed"
                ValidationExpression="^[1-9]\d*(\.\d+)?$" />
            <asp:Button ID="btnClr" runat="server" OnClick="btnClr_Click" Text="Clear" />
            
        </div>

        <p>
            <asp:Button ID="btnSum" runat="server" OnClick="btnSum_Click" Text="+" Width="45px" />
            <asp:Button ID="btnMinus" runat="server" OnClick="btnMinus_Click" Text="-" Width="45px" />
            <asp:Button ID="btnMultiply" runat="server" OnClick="btnMultiply_Click" Text="*" Width="45px" />
            <asp:Button ID="btnDivide" runat="server" OnClick="btnDivide_Click" Text="/" Width="45px" />
        </p>
        <p>
            <asp:Label ID="Label3" runat="server" Text="Result : "></asp:Label>
            <asp:TextBox ID="txtResult" runat="server"></asp:TextBox>
        </p>
    </form>
</body>
</html>
