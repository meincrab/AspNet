﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace task4
{
    public partial class CityList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string selectedText = ddlTest.SelectedItem.Text;
            string selectedValue = ddlTest.SelectedItem.Value;
        }

        protected void ddlTest_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedText = ddlTest.SelectedItem.Text;
            string selectedValue = ddlTest.SelectedItem.Value;

            //--- Show results in page.
            Response.Write("Selected Text is " + selectedText + " and selected value is :" + selectedValue);
        }

        protected void addBtn_Click(object sender, EventArgs e)
        {
            ListItem li = new ListItem();
            li.Text = addCity.Text.ToString();
            if (!string.IsNullOrEmpty(addCity.Text.ToString()))
            {
                int maxValue = ddlTest.Items.Cast<ListItem>().Select(item => int.Parse(item.Value)).Max();
                li.Value = (maxValue + 1).ToString();
                ddlTest.Items.Add(li);
                Label1.Text = "New city Added: " + addCity.Text.ToString();
                inputError.Text = "";
            }
            else
            {
                inputError.Text = "Empty input!";
            }
            
        }

        protected void rmvCity_Click(object sender, EventArgs e)
        {
            try
            {
                ddlTest.Items.RemoveAt(Int32.Parse(ddlTest.SelectedItem.Value));
            }
            catch(ArgumentOutOfRangeException)
            {
                ddlTest.Items.RemoveAt(0);
            }
           
        }

        protected void renBtn_Click(object sender, EventArgs e)
        {
            string selectedValue = ddlTest.SelectedItem.Value;
            ddlTest.Items.FindByValue(selectedValue).Text = addCity.Text.ToString();
        }
    }
}