﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CityList.aspx.cs" Inherits="task4.CityList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body id="Label1">
    <form id="form1" runat="server">
        <div>
            <asp:dropdownlist runat="server" autopostback="true" onselectedindexchanged="ddlTest_SelectedIndexChanged" id="ddlTest">
                <asp:listitem text="Tripoli" value="0"></asp:listitem>
                <asp:listitem text="Zahle" value="1"></asp:listitem>
                <asp:listitem text="Maseru" value="2"></asp:listitem>
                <asp:listitem text="Dushanbe" value="3"></asp:listitem>
                <asp:listitem text="Fergana" value="4"></asp:listitem>
            </asp:dropdownlist>
            
            <asp:Label ID="Label1" runat="server"></asp:Label>
            
        </div>
        <asp:TextBox ID="addCity" runat="server" Width="530px"></asp:TextBox>
        <asp:Label ID="inputError" runat="server"></asp:Label>
        <br />
        <p>
        <asp:Button ID="addBtn" runat="server" Text="Add City" OnClick="addBtn_Click" Width="179px" />
        </p>
        <p>
            <asp:Button ID="renBtn" runat="server" OnClick="renBtn_Click" Text="Rename Current City" />
        </p>
        <p>
            <asp:Button ID="rmvCity" runat="server" OnClick="rmvCity_Click" Text="Remove Current City" />
        </p>
    </form>
</body>
</html>
