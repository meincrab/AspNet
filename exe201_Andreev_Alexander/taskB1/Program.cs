﻿using System;
/*
 * B1 Create this program:  programs uses Ohms law to calculate the resistance. User gives voltage and current.
 * Assuming what input should be positive
 * */
namespace taskB1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter voltage : ");
            string usrInputVolt = Console.ReadLine();
            Console.Write("Enter current : ");
            string usrInputCur = Console.ReadLine();

            float voltage, current, resistance;
            voltage = current = resistance = 0;

            if (!float.TryParse(usrInputVolt, out voltage))
            {
                voltage = -1;
            }

            if (!float.TryParse(usrInputCur, out current))
            {
                current = -1;
            }

            if(voltage < 0 | current < 0)
            {
                Console.WriteLine("Wrong input given");
            }
            else
            {
                resistance = voltage / current;
                Console.WriteLine("Resistance is {0} ohm", resistance);
                Console.Read();
            }
        }
    }
}
