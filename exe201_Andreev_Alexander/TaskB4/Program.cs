﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 B4 Amount of euros is given in a variable. Program converts euros to 5, 10, 20, 50 euros bills.

    E.g. User gives 99: the result 1 50 euros bill, 2 20 euros bills, 1 5 euros bill and remaining 4 euros.

 */

namespace TaskB4
{
    class Program
    {
        static void Main(string[] args)
        {
            long moneyAmount = 0;
            Console.WriteLine("Enter Amount of money");
            string usrInputMoney = Console.ReadLine();
            if (Int64.TryParse(usrInputMoney, out moneyAmount))
            {
                long bill50 = 0;
                byte bill20 = 0;
                byte bill5 = 0;

                while (moneyAmount >= 50)
                {
                    moneyAmount -= 50;
                    bill50 += 1;
                }

                while (moneyAmount >= 20)
                {
                    moneyAmount -= 20;
                    bill20 += 1;
                }

                while (moneyAmount >= 5)
                {
                    moneyAmount -= 5;
                    bill5 += 1;
                }

                Console.WriteLine("Your money amount was {0} , result : 50 euro bills : {1} , 20 euro bills : {2} , 5 euro bills : {3} and remains : {4} euros", usrInputMoney, bill50, bill20, bill5, moneyAmount);
                Console.Read();

            }


        }
    }
}
