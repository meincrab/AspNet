﻿using System;
/* 
B7 A stone is dropped down from the top of Pisa tower. What is the final speed of the stone and how much time does the fall take? 
*/
namespace taskB7
{
    class Program
    {
        static void Main(string[] args)
        {
            const float pisaHeight = 58.36f;
            const float gravAccel = 9.807f;
            double endSpeed = Math.Sqrt(2 * pisaHeight * gravAccel);
            double fallTime = Math.Sqrt((2 * pisaHeight) / gravAccel);
            Console.WriteLine("Final speed is : " + Math.Round(endSpeed,3) + " m/s and fall time is : " + Math.Round(fallTime,2) + " sec");

        }
    }
}
