﻿using System;
/* E1 Calculates the sum of 2 integers and prints out the result. */
namespace E1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter first Integer\n");
            int num1 = -1;
            while (!int.TryParse(Console.ReadLine(), out num1))
                Console.WriteLine("Try again");
            int num2 = -1;
            Console.Write("Enter second Integer\n");
            while (!int.TryParse(Console.ReadLine(), out num2))
                Console.WriteLine("Try again");

            Program.SumNumbers(num1, num2);
        }

        public static void SumNumbers(int num1, int num2)
        {
            Console.WriteLine(num1 + num2);
        }
    }
}
