﻿using System;

namespace E4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter first number\n");
            float num1 = -1;
            while (!float.TryParse(Console.ReadLine(), out num1))
                Console.WriteLine("Try again");
            float num2 = -1;
            Console.Write("Enter second number\n");
            while (!float.TryParse(Console.ReadLine(), out num2))
                Console.WriteLine("Try again");
            float num3 = -1;
            Console.Write("Enter third number\n");
            while (!float.TryParse(Console.ReadLine(), out num3))
                Console.WriteLine("Try again");
            float num4 = -1;
            Console.Write("Enter fourth number\n");
            while (!float.TryParse(Console.ReadLine(), out num4))
                Console.WriteLine("Try again");

            Program.Average(num1, num2, num3, num4);
            Console.WriteLine("Your average: ");
            Console.WriteLine(Program.Average(num1, num2, num3, num4));

        }

        public static double Average(float luku1, float luku2, float luku3, float luku4)
        {
            double average = (double)(luku1 + luku2 + luku3 + luku4) / 4;
            return average;
        }
    }
}
