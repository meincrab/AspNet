﻿using System;
/* E3 Returns the average of 2 integers */
namespace E3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter first Integer\n");
            int num1 = -1;
            while (!int.TryParse(Console.ReadLine(), out num1))
                Console.WriteLine("Try again");
            int num2 = -1;
            Console.Write("Enter second Integer\n");
            while (!int.TryParse(Console.ReadLine(), out num2))
                Console.WriteLine("Try again");

            Program.Average(num1, num2);
            Console.WriteLine(Program.Average(num1, num2));
        }

        public static float Average (int luku1, int luku2){
            float average = (float)(luku1 + luku2) / 2;
            return average;
        }
    }
}
