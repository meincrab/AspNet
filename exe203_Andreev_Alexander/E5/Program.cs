﻿using System;

namespace E5
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            Console.WriteLine(Program.Average(array));
        }

        public static int Average(int[] arr)
        {
            int arraySum = 0;
            foreach (int elem in arr)
            {
                arraySum += elem;
            }
            return arraySum;
        }
    }
}
