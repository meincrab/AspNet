﻿using System;

namespace D1
{
    /* D1 Create and fill an array that can store: 
     * 

    a) 4 citynames

    a) 500 decimal numbers

     */
    class Program
    {
        static void Main(string[] args)
        {
            string[] cityNames = { "London", "Simpheropol", "Helsinki", "Karaganda" };

            foreach (var city in cityNames)
            {
                Console.Write(city + " ");
            }

            float[] numberArray = new float[500];

            Console.WriteLine("");
            for (int i = 0; i < 500; i++)
            {
                numberArray[i] = i;
            }
            
            foreach(var number in numberArray)
            {
                Console.Write(number + " ");
            }


            Console.ReadKey();
        }
    }
}
