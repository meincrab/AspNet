﻿using System;

/*D2 Create an array to store 6 Greek letters and correspondent western letters.*/
namespace D2
{
    class Program
    {
        static void Main(string[] args)
        {
            string[,] lettersArray = new string[6, 2]
            {
                {"\u03B1", "a"},
                {"\u03B2", "b"},
                {"\u03B3", "g"},
                {"\u03B4", "d"},
                {"\u03B5", "e"},
                {"\u03B6", "d+"}
            };


            int rowLength = lettersArray.GetLength(0);
            int colLength = lettersArray.GetLength(1);

            for (int i = 0; i < rowLength; i++)
            {
                for (int j = 0; j < colLength; j++)
                {
                    Console.Write(string.Format("{0} ", lettersArray[i, j]));
                }
                Console.WriteLine(Environment.NewLine + Environment.NewLine);
            }
        }
    }
}