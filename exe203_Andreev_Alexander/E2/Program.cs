﻿using System;
/* E2 Returns the sum of 2 integers. */
namespace E2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter first Integer\n");
            int num1 = -1;
            while (!int.TryParse(Console.ReadLine(), out num1))
                Console.WriteLine("Try again");
            int num2 = -1;
            Console.Write("Enter second Integer\n");
            while (!int.TryParse(Console.ReadLine(), out num2))
                Console.WriteLine("Try again");

            Console.WriteLine(Program.ReturnSum(num1, num2)); 
        }
        public static int ReturnSum (int num1, int num2)
        {
            int sum = num1 + num2;
            return sum;
        }
    }
}
