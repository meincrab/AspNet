﻿using System;
/* 
 D4 Array algorithms -tasks:

    Filling an array with random numbers

    Printing out array values.

    Calculate the sum and the average.

    Searching for the minimun/maximum value

    Checking if a specific value is in the table

    Sorting an array
*/

namespace D4
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int arrayLength = rand.Next(0,10);
            int[] randomArray = new int[arrayLength];
            for(int i = 0; i < randomArray.Length; i++)
            {
                randomArray[i] = rand.Next(0,10);
            }
            long sumOfArray = 0;
            int minValue = randomArray[0];
            int maxValue = randomArray[0];
            int valueToCheck = 8;
            bool valuePresent = false;
            foreach (int number in randomArray)
            {
                Console.WriteLine(number);
                sumOfArray += number;
                if (number == valueToCheck) valuePresent = true;
                if (minValue >= number) minValue = number;
                if (maxValue <= number) maxValue = number;
            }


            Console.WriteLine("Array Length ");
            Console.WriteLine(randomArray.Length);
            Console.WriteLine("Array Sum");
            Console.WriteLine(sumOfArray);
            Console.WriteLine("Array average:");
            float c = (float) sumOfArray / randomArray.Length;
            Console.WriteLine(c);        
            Console.WriteLine("Minimum value: ");
            Console.WriteLine(minValue);
            Console.WriteLine("Maximum value: ");
            Console.WriteLine(maxValue);
            if (valuePresent) Console.WriteLine("your number {0} is present in the array", valueToCheck);
            if(!valuePresent) Console.WriteLine("your number {0} is not present in the array", valueToCheck);
            Console.WriteLine("For sorting in this exercize I wll be use bubblesort. Unefficient, slow, but still easy to implement");
            Console.WriteLine("Sorted array is : ");
            int temp;
            for (int j = 0; j <= randomArray.Length - 2; j++)
            {
                for (int i = 0; i <= randomArray.Length - 2; i++)
                {
                    if(randomArray[i] > randomArray[i + 1])
                    {
                        temp = randomArray[i + 1];
                        randomArray[i + 1] = randomArray[i];
                        randomArray[i] = temp;
                    }
                }
            }
            Console.WriteLine("Sorted Array: ");
            foreach(int number in randomArray)
            {
                Console.WriteLine(number);
            }
        }
    }
}
